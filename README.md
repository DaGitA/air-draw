This is the project for GIF-4100 Digital Vision at Laval University

The goal is to draw in 3D with your index. For the moment we detect only your hand.

## Technologies:
* OpenCVSharp (OpenCV C# wrapper)
* Kinect with Kinect SDK 1.8
* Unity

## Details
* 1 meter = 1 Unity unit
