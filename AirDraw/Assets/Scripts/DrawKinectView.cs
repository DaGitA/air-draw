﻿using UnityEngine;
using UnityEngine.UI;

//TODO refact in DrawCameraView
public class DrawKinectView : MonoBehaviour
{
    private Texture2D texture;
    private KinectVideoFeedCapture kinect;

    // Use this for initialization
    void Start()
    {
        Main main = GameObject.Find("GameModule").GetComponent<Main>();
        DeviceOrEmulator devOrEmu = main.GetKinectDeviceOrEmulator();
        kinect = new KinectVideoFeedCapture(devOrEmu);
        texture = new Texture2D(320, 240, TextureFormat.ARGB32, false);
        GetComponent<RawImage>().texture = texture;
    }

    // Update is called once per frame
    void Update()
    {
        if (kinect.pollColor())
        {
            Color32[] pixels = kinect.GetCurrentColor();
            texture.SetPixels32(pixels);
            texture.Apply();
        }
    }
}
