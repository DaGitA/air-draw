﻿using UnityEngine;

public class HandSelector : MonoBehaviour {

    private DrawModule drawModule;
    private VisionController visionController;
    private Main main;

	void Start () {
        this.drawModule = GetComponent<DrawModule>();
        this.main = GetComponent<Main>();
        this.visionController = main.GetVisionController();
    }
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            drawModule.handManuallySelected = true;
            visionController.useCurrentHand();
        }
	}

    
}
