﻿using System;
using UnityEngine;

public class Index : MonoBehaviour
{
    private VisionController visionController;

    void Start()
    {
        Main main = GameObject.Find("GameModule").GetComponent<Main>();
        this.visionController = main.GetVisionController();
    }

    void Update()
    {
        if (visionController == null) return;
        Vector3? indexPosition = visionController.GetIndexPoint();
        if (indexPositionShouldChange(indexPosition))
        {
            //Debug.Log(indexPosition.Value);
            transform.position = indexPosition.Value;
        }
        else
        {
            Debug.Log("Vision controller returned no index position");
        }
    }

    private bool indexPositionShouldChange(Vector3? indexPosition)
    {
        return indexPosition.HasValue && indexPosition.Value.x != 0 && indexPosition.Value.y != 0 && indexPosition.Value.z != 0;
    }
}
