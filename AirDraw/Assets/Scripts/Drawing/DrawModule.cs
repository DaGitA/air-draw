﻿using System.Collections.Generic;
using UnityEngine;

public class DrawModule : MonoBehaviour
{
    Vector3 currentIndexPosition;
    public GameObject indexPrefab;
    VisionController visionController;
    Vector3 initialIndexPosition;
    [HideInInspector]
    public bool handManuallySelected;

    void Start()
    {
        handManuallySelected = false;
        Index index = indexPrefab.GetComponent<Index>();
        Main main = GameObject.Find("GameModule").GetComponent<Main>();
        visionController = main.GetVisionController();
        initialIndexPosition = indexPrefab.transform.position;
    }

    void Update()
    {
        Vector3 newIndexPosition = indexPrefab.transform.position;

        if (shouldDraw(newIndexPosition))
        {
            GameObject line = new GameObject("line");
            LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
            lineRenderer.SetWidth(0.02f, 0.02f);
            if(currentIndexPosition == Vector3.zero)
            {
                currentIndexPosition = newIndexPosition;
            }
            lineRenderer.SetPosition(0, currentIndexPosition);
            lineRenderer.SetPosition(1, newIndexPosition);

            currentIndexPosition = newIndexPosition;
        }

        if (Input.GetKeyDown(KeyCode.Delete))
        {
            eraseAll();
        }
    }

    private bool shouldDraw(Vector3 newIndexPosition)
    {
        return handManuallySelected && newIndexPosition != currentIndexPosition && visionController.handIsInDrawingPosition() && newIndexPosition != initialIndexPosition;
    }

    private void eraseAll()
    {
        LineRenderer[] list = FindObjectsOfType<LineRenderer>();
        for(int i = 0; i < list.Length; i++)
        {
            Destroy(list[i].gameObject);
        }
    }
}
