﻿using UnityEngine;
using Assets.Scripts;
using AirDraw.Filters;
using OpenCvSharp;
using Assets.Scripts.Vision.VideoCapture;
using Assets.Scripts.Vision;

public class Main : MonoBehaviour {

    public bool useKinectForVideoCapture;
    public DeviceOrEmulator devOrEmu;
    public BoxCollider workspaceBounds;
    private VisionController visionController;
    private Mat lastFrame;
    private bool Calibrating = true;
    private Window calibration;
    private VideoFeedCapture videoCaptureHardware;
    private Calibrator calibrator;

    Settings defaultSettings = new Settings()
    {
        hueLow = 175,
        satLow = 25,
        briLow = 150,
        hueHigh = 20,
        satHigh = 200,
        briHigh = 255
    };

    void Awake () {
        DepthCapture depthCapture;

        if (useKinectForVideoCapture)
        {
            videoCaptureHardware = new KinectVideoFeedCapture(devOrEmu);
            depthCapture = new KinectDepthCapture(devOrEmu);
        }
        else
        {
            int defaultCamera = 0;
            videoCaptureHardware = new CameraVideoFeedCapture(defaultCamera);
            depthCapture = new CameraDepthCapture();
        }

        visionController = new VisionController(videoCaptureHardware, defaultSettings, depthCapture, GetWorkspaceBounds());
        calibrator = new Calibrator(videoCaptureHardware, visionController);
    }

    void Update()
    {
        calibrator.Update();
    }

    void OnDestroy()
    {
        Cv2.DestroyAllWindows();
    }

    internal DeviceOrEmulator GetKinectDeviceOrEmulator()
    {
        return devOrEmu;
    }

    internal VisionController GetVisionController()
    {
        return visionController;
    }

    private Vector3 GetWorkspaceBounds()
    {
        return workspaceBounds.bounds.size;
    }

   
}
