﻿using System.Collections;
using System.Collections.Generic;
using AirDraw.Filters;
using Assets.Scripts;
using OpenCvSharp;
using UnityEngine;

public class VaubMain : MonoBehaviour {

    public DeviceOrEmulator devOrEmu;

	Settings settings = new Settings() {
		hueLow = 0,
		satLow = 50,
		briLow = 50,
		hueHigh = 20,
		satHigh = 230,
		briHigh = 255
	};

    Assets.Scripts.HandTracker handTracker = new Assets.Scripts.HandTracker();

	// Use this for initialization
	void Start () {
		Debug.Log("Starting OpenCV engine");
		var camera = new CameraVideoFeedCapture(0);
        //var camera = new KinectVideoFeedCapture(devOrEmu);

		Cv2.NamedWindow("original");
        //Cv2.NamedWindow("preview");

        Cv2.NamedWindow("skin");
        Cv2.NamedWindow("morph");
        Cv2.NamedWindow("contours");

		try {
			captureLoop(camera);
		} catch (CouldNotCaptureFrameException e) {
			Debug.Log("Frame lost");
		} finally {
			Debug.Log("Camera was closed");
		}

        Cv2.DestroyAllWindows();
	}

	void captureLoop(VideoFeedCapture feed) {
		while (true) {
			var frame = feed.CaptureFrame();
			trackHand(frame);

			int key = Cv2.WaitKey(5);
			if (key == 'h' || key == 'q') {
				if (key == 'h') {
					handTracker.UseCurrent();
				}

				if (key == 'q') {
					break;
				}
			}
		}
	}

	void trackHand(Mat output) {
		var filters = new Filters(output, settings);

		var filtered = 
			filters.OutputDebug("original")
			.ApplySkinExtraction().OutputDebug("skin")
			.ApplyMorphology().OutputDebug("morph")
			.GetFrame();

		handTracker.MatchHand(filtered).OutputDebug("contours", output);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
