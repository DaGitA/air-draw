﻿using AirDraw.Filters;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class HandTracker
    {
        private Hand? hand;
        private Point? lastPoint;
        private Mat handOutput;

        private ContourShape[] shapes;
        private Point[][] contours;

        public HandTracker()
        {
            lastPoint = null;
        }

        public Point? GetHandCenter()
        {
            if (lastPoint.HasValue)
            {
                return lastPoint.Value;
            }
            return null;
        }

        public HandTracker MatchHand(Mat image)
        {
            shapes = findContours(image);

            if (shapes.Length != 0)
            {
                hand = findHand(shapes);
            }

            return this;
        }

        private Hand findHand(ContourShape[] contours)
        {
            ContourShape choosen = contours[0];
            double score = 0;

            foreach (var contour in contours)
            {
                var hull = Cv2.ConvexHullIndices(contour.poly);

                //Debug.Log("Contour: " + contour.contour.Length + " Hull: " + contour.hull.Length);
                var defects = Cv2.ConvexityDefects(contour.poly, hull);
                var nbDefects = defects.Length;

                double areaWeight = contour.area / 200;
                int idealNumberOfDefects = 7;
                double distanceWithIdealNumberOfDefects = Math.Abs(idealNumberOfDefects - nbDefects);
                double defectsScore = distanceWithIdealNumberOfDefects == 0 ? 1 : 1 / distanceWithIdealNumberOfDefects;

                double distanceScore = 0;
                if (this.lastPoint.HasValue)
                {
                    Point center = contour.FindCenter();
                    double distance = Math.Abs(Point.Distance(center, this.lastPoint.Value));

                    distanceScore = distance == 0 ? 1 : 1 / (1 + distance);
                }

                Debug.Log("Distance " + 10000 * distanceScore);
                double currentScore = (10000 * distanceScore) + (areaWeight) + (500 * defectsScore);

                if (currentScore > score)
                {
                    choosen = contour;
                    score = currentScore;
                }
            }

            var choosenCenter = choosen.FindCenter();
            if (lastPoint.HasValue)
            {
                lastPoint = choosenCenter;
            }

            return new Hand() { shape = choosen, hull = Cv2.ConvexHull(choosen.hull), center = choosenCenter, radius = choosen.FindRadius(choosenCenter) };
        }

        private ContourShape[] findContours(Mat image)
        {
            Point[] found = new Point[0];
            Point[][] contours = this.contours = image.FindContoursAsArray(RetrievalModes.External, ContourApproximationModes.ApproxSimple);
            Mat[] contoursMat = image.FindContoursAsMat(RetrievalModes.External, ContourApproximationModes.ApproxSimple);

            if (contours.Length == 0)
            {
                return new ContourShape[0];
            }

            var contoursFound = new List<ContourShape>();
            for (int i = 0; i < contours.Length; ++i)
            {
                var area = Math.Abs(Cv2.ContourArea(contours[i]));

                var poly = Cv2.ApproxPolyDP(contours[i], 2, closed: true);
                var hull = Cv2.ConvexHull(contours[i]);

                //Debug.Log("Contour " + contours[i].Length + " Poly " + poly.Length + " Hull " + hull.Length);
                contoursFound.Add(new ContourShape()
                {
                    area = area,
                    poly = poly,
                    hull = hull,
                    contourMat = contoursMat[i]
                });
            }

            return contoursFound.ToArray();
        }

        public void UseCurrent()
        {
            if (!this.hand.HasValue)
            {
                return;
            }

            var hand = this.hand.Value;
            lastPoint = hand.center;
        }

        public void OutputDebug(string window, Mat image)
        {

            if (!this.hand.HasValue)
            {
                return;
            }
            Hand hand = this.hand.Value;

            if (hand.shape.hull.Length < 1)
            {
                return;
            }

            var shape = shapes.Select(s => s.hull).ToArray();
            var hull = new Point[1][] { hand.hull };

            Cv2.DrawContours(image, contours, -1, Scalar.LightGreen, thickness: 1);
            Cv2.DrawContours(image, shape, -1, Scalar.Magenta, thickness: 2);
            Cv2.DrawContours(image, hull, 0, Scalar.Blue, thickness: 3);

            Cv2.Circle(image, hand.center.X, hand.center.Y, radius: 5, color: Scalar.LimeGreen, thickness: 2);
            Cv2.Circle(image, hand.center.X, hand.center.Y, radius: (int)hand.radius, color: Scalar.LimeGreen, thickness: 2);

            if (lastPoint.HasValue)
            {
                var point = lastPoint.Value;
                Cv2.Circle(image, point.X, point.Y, radius: 5, color: Scalar.Red, thickness: 2);
            }

            Cv2.ImShow(window, image);
        }

    }
}
