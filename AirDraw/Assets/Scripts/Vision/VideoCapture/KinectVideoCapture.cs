﻿using Assets.Scripts;
using OpenCvSharp;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class KinectVideoFeedCapture : VideoFeedCapture
{
    private DeviceOrEmulator devOrEmu;
    private Kinect.KinectInterface kinect;

    public KinectVideoFeedCapture(DeviceOrEmulator devOrEmu)
    {
        this.devOrEmu = devOrEmu;
        kinect = devOrEmu.getKinect();
    }

    public Mat CaptureFrame()
    {
        if (kinect.pollColor())
        {
            Color32[] pixels = kinect.getColor();
            byte[] pixelsInByte = new byte[640 * 480 * 3];
            ConvertRGBPixelsInBGRBytes(pixels, pixelsInByte);

            return new Mat(480, 640, MatType.CV_8UC3, pixelsInByte);
        }
        else
        {
            throw new CouldNotCaptureFrameException();
        }
    }

    private static void ConvertRGBPixelsInBGRBytes(Color32[] pixels, byte[] pixelsInByte)
    {
        for (int i = 0; i < 640 * 480; ++i)
        {
            int idx = i * 3;
            pixelsInByte[idx + 0] = pixels[i].b;
            pixelsInByte[idx + 1] = pixels[i].g;
            pixelsInByte[idx + 2] = pixels[i].r;
        }
    }

    private static byte[] Color32ArrayToByteArray(Color32[] colors)
    {
        if (colors == null || colors.Length == 0)
            return null;

        int lengthOfColor32 = Marshal.SizeOf(typeof(Color32));
        int length = lengthOfColor32 * colors.Length;
        byte[] bytes = new byte[length];

        GCHandle handle = default(GCHandle);
        try
        {
            handle = GCHandle.Alloc(colors, GCHandleType.Pinned);
            IntPtr ptr = handle.AddrOfPinnedObject();
            Marshal.Copy(ptr, bytes, 0, length);
        }
        finally
        {
            if (handle != default(GCHandle))
                handle.Free();
        }

        return bytes;
    }

    private Color32[] mipmapImg(Color32[] src, int width, int height)
    {
        int newWidth = width / 2;
        int newHeight = height / 2;
        Color32[] dst = new Color32[newWidth * newHeight];
        for (int yy = 0; yy < newHeight; yy++)
        {
            for (int xx = 0; xx < newWidth; xx++)
            {
                int TLidx = (xx * 2) + yy * 2 * width;
                int TRidx = (xx * 2 + 1) + yy * width * 2;
                int BLidx = (xx * 2) + (yy * 2 + 1) * width;
                int BRidx = (xx * 2 + 1) + (yy * 2 + 1) * width;
                dst[xx + yy * newWidth] = Color32.Lerp(Color32.Lerp(src[BLidx], src[BRidx], .5F),
                                                       Color32.Lerp(src[TLidx], src[TRidx], .5F), .5F);
            }
        }
        return dst;
    }

    public Texture2D GetCurrentTexture()
    {
        Texture2D t = new Texture2D(320, 240, TextureFormat.ARGB32, false);
        t.SetPixels32(mipmapImg(kinect.getColor(), 640, 480));
        t.Apply(false);
        return t;
    }

    public Color32[] GetCurrentColor()
    {
        //Texture2D t = new Texture2D(320, 240, TextureFormat.ARGB32, false);
        return mipmapImg(kinect.getColor(), 640, 480);
    }

    public bool pollColor()
    {
        return kinect.pollColor();
    }

    public Vector3 ScaleForWorkspace(Vector3 workspaceDimensions, Vector3 results)
    {
        results.x = (results.x * workspaceDimensions.x) / 640;
        results.y = (results.y * workspaceDimensions.y) / 480;
        //z is initially in millimeters according to Microsoft.
        float z = results.z / 1000;
        z = Mathf.Clamp(z, 6, 8); //Experimental values
        results.z = (z - 6) / 2;

        //Flip to be on the same side of the user.
        results.y = workspaceDimensions.y - results.y;
        results.z = workspaceDimensions.z - results.z;
        return results;
    }
}
