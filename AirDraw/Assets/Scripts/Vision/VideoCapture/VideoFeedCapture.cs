﻿using OpenCvSharp;
using System;
using UnityEngine;

namespace Assets.Scripts
{
    public interface VideoFeedCapture
    {
        Mat CaptureFrame();

        Vector3 ScaleForWorkspace(Vector3 workspaceDimensions, Vector3 results);
    }
}
