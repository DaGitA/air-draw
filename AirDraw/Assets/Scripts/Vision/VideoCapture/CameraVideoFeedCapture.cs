﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using OpenCvSharp;
using System;

/// <exception cref="CouldNotStartVideoFeedException">
/// </exception>
public class CameraVideoFeedCapture : VideoFeedCapture
{
    private readonly VideoCapture _videoCapture;

    public CameraVideoFeedCapture(int camera)
    {
        Debug.Log("Starting OpenCV engine");
        _videoCapture = new VideoCapture(camera);
        if (!_videoCapture.IsOpened())
        {
            throw new CouldNotStartVideoFeedException();
        }

        Debug.Log("Hue at " + _videoCapture.Hue);
    }

    public Mat CaptureFrame()
    {
        Mat frame = new Mat();
        bool isCaptured = _videoCapture.Read(frame);
        if (!isCaptured) {
            throw new CouldNotCaptureFrameException();
        }

        return frame;
    }

    public Vector3 ScaleForWorkspace(Vector3 workspaceDimensions, Vector3 results)
    {
        results.x = (results.x * workspaceDimensions.x) / 640;
        results.y = (results.y * workspaceDimensions.y) / 480;

        //Flip to be on the same side of the user.
        results.x = workspaceDimensions.x - results.x;
        results.y = workspaceDimensions.y - results.y;
        return results;
    }
}
