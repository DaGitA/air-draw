﻿using UnityEngine;
using System.Collections;

public class KinectDepthCapture : DepthCapture {

    const int INPUT_IMAGE_WIDTH = 640;
    const int INPUT_IMAGE_HEIGHT = 480;

    const int DEPTH_IMAGE_WIDTH = 320;
    const int DEPTH_IMAGE_HEIGHT = 240;

    private DeviceOrEmulator devOrEmu;
    private Kinect.KinectInterface kinect;

    public KinectDepthCapture(DeviceOrEmulator devOrEmu)
    {
        this.devOrEmu = devOrEmu;
        kinect = devOrEmu.getKinect();
    }

    public float GetDepth(int x, int y)
    {
        short[] depthImage = getDepthImage();
        scaleAtDepthImageResolution(ref x, ref y);
        int index = x + (y * DEPTH_IMAGE_WIDTH);

        if (index > (DEPTH_IMAGE_WIDTH * DEPTH_IMAGE_HEIGHT))
        {
            throw new CouldNotFindDepthException();
        }

        return depthImage[index];
    }

    private static void scaleAtDepthImageResolution(ref int x, ref int y)
    {
        x = (x * DEPTH_IMAGE_WIDTH) / INPUT_IMAGE_WIDTH;
        y = (y * DEPTH_IMAGE_HEIGHT) / INPUT_IMAGE_HEIGHT;
    }

    private short[] getDepthImage()
    {
        short[] depthImage = new short[DEPTH_IMAGE_WIDTH * DEPTH_IMAGE_HEIGHT];
        
        if (kinect.pollDepth())
		{
            depthImage = kinect.getDepth();
        }
      
        return depthImage;
    }
}
