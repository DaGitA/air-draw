﻿using System;

namespace Assets.Scripts
{
    public class CouldNotStartVideoFeedException : Exception { }
}
