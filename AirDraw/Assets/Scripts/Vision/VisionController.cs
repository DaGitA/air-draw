﻿using UnityEngine;
using Assets.Scripts;
using OpenCvSharp;
using AirDraw.Filters;
using System;

public class VisionController
{
    private VideoFeedCapture videoFeed;
    private HandTracker handTracker;
    private Settings settings;
    private DepthCapture depthCapture;
    private Vector3 workspaceDimensions;

    public VisionController(VideoFeedCapture videoFeed, Settings settings, DepthCapture depthCapture, Vector3 workspaceDimensions)
    {
        this.videoFeed = videoFeed;
        this.handTracker = new HandTracker();
        this.settings = settings;
        this.depthCapture = depthCapture;
        this.workspaceDimensions = workspaceDimensions;
    }

    internal void useCurrentHand()
    {
        handTracker.UseCurrent();
    }

    public Vector3? GetIndexPoint()
    {
        var frame = videoFeed.CaptureFrame();
        var filters = new Filters(frame, settings);

        var filtered =
            filters
            .ApplySkinExtraction().OutputDebug("skin")
            .ApplyMorphology().OutputDebug("morph")
            .GetFrame();

        handTracker.MatchHand(filtered).OutputDebug("contours", frame);

        Point? handCenter = handTracker.GetHandCenter();
        return AddDepthValue(handCenter);
    }

    private Vector3? AddDepthValue(Point? handCenter)
    {
        if (handCenter.HasValue)
        {
            float zValue = depthCapture.GetDepth(handCenter.Value.X, handCenter.Value.Y);
            Vector3 results = new Vector3(handCenter.Value.X, handCenter.Value.Y, zValue);
            results = videoFeed.ScaleForWorkspace(workspaceDimensions, results);

            return results;
        }
        else
        {
            return null;
        }
    }

    public void changeSettings(Settings settings)
    {
        this.settings = settings;
    }

    public bool handIsInDrawingPosition()
    {
        //TODO Detect gesture
        return true;
    }
}
