﻿using UnityEngine;
using System.Collections;
using OpenCvSharp;

public struct Hand
{
    public ContourShape shape;
    public Point[] hull;
    public Point center;
    public float radius;
}