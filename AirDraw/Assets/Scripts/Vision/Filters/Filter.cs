using System;
using System.Collections.Generic;
using OpenCvSharp;
using UnityEngine;

namespace AirDraw.Filters {

    public class Filters {

        private readonly Settings settings;

        private Mat frame;
        private Mat outputFrame;

        public Filters(Mat frame, Settings settings) {
            this.settings = settings;
            this.frame = this.outputFrame = frame;
        }

        public Filters ApplySkinExtraction() {

            Debug.Log("LOW H " + settings.hueLow + " S " + settings.satLow + " V " + settings.briLow);
            Debug.Log("HIGH H " + settings.hueHigh + " S " + settings.satHigh + " V " + settings.briHigh);

            frame = frame
                //.BilateralFilter(15, 2, 2)
                .GaussianBlur(new Size(11, 11), 0)
                .MedianBlur(11)
                .CvtColor(ColorConversionCodes.BGR2HSV)
                ;
            
            if (settings.hueLow > settings.hueHigh) // taking in account chroma circle for hue
            {
                var lowerBound1 = InputArray.Create(new int[] { settings.hueLow, settings.satLow, settings.briLow });
                var upperBound1 = InputArray.Create(new int[] { 179, settings.satHigh, settings.briHigh });

                var lowerBound2 = InputArray.Create(new int[] { 0, settings.satLow, settings.briLow });
                var upperBound2 = InputArray.Create(new int[] { settings.hueHigh, settings.satHigh, settings.briHigh });

                frame = frame.InRange(lowerBound1, upperBound1) | frame.InRange(lowerBound2, upperBound2);
            }
            else
            {
                var lowerBound = InputArray.Create(new int[] { settings.hueLow, settings.satLow, settings.briLow });
                var upperBound = InputArray.Create(new int[] { settings.hueHigh, settings.satHigh, settings.briHigh });

                frame = frame.InRange(lowerBound, upperBound);
            }

            outputFrame = frame;
            return this;
        }

        public Filters ApplyMorphology() {
            var smallKernel = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(3, 3));
            var bigKernel = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(5, 5), new Point(4, 4));

            frame = frame
                //.MorphologyEx(MorphTypes.ERODE, smallKernel)
                .MorphologyEx(MorphTypes.Close, bigKernel)
                //.MorphologyEx(MorphTypes.Close, bigKernel)
                .GaussianBlur(new Size(3, 3), 0)
                ;
            outputFrame = frame;
            
            return this;
        }

        public Mat GetFrame() {
            return frame;
        }

        public Filters OutputDebug(string window) {
            Cv2.ImShow(window, outputFrame);
            return this;
        }
    }

}

