﻿public struct Settings
{
    public Settings(int hueLow, int hueHigh, int satLow, int satHigh, int briLow, int briHigh)
    {
        this.hueLow = normalizeHue(hueLow);
        this.hueHigh = normalizeHue(hueHigh);
        this.satLow = normalizeByte(satLow);
        this.satHigh = normalizeByte(satHigh);
        this.briLow = normalizeByte(briLow);
        this.briHigh = normalizeByte(briHigh);
    }

    private static int normalizeHue(int value)
    {
        return value > 179 ? value - 180 :
               value < 0 ? 180 + value :
               value;
    }

    private static int normalizeByte(int value)
    {
        return value > 255 ? 255 :
               value < 0 ? 0 :
               value;
    }

    public int hueLow, hueHigh;
    public int satLow, satHigh;
    public int briLow, briHigh;
}
