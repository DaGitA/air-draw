﻿using UnityEngine;
using System.Collections;
using OpenCvSharp;

public struct ContourShape
{
    public double area;
    public Point[] poly;
    public Point[] hull;
    public Mat contourMat;

    private Vec4i[] _defects;

    public Point FindCenter()
    {
        Moments M = Cv2.Moments(contourMat, true);
        int cX = (int)(M.M10 / M.M00);
        int cY = (int)(M.M01 / M.M00);

        return new Point(cX, cY);
        //findDefects();

        //if (_defects.Length < 1)
        //{
        //    return new Point(0, 0);
        //}

        //int x = 0, y = 0;
        //foreach (var defect in _defects)
        //{
        //    Point d = contour[defect.Item2];
        //    x += d.X;
        //    y += d.Y;
        //}

        //return new Point(x / _defects.Length, y / _defects.Length);
    }

    public float FindRadius(Point center)
    {
        findDefects();

        if (_defects.Length < 1)
        {
            return 0;
        }

        float d2 = 0;
        foreach (var defect in _defects)
        {
            Point d = poly[defect.Item2];

            int x = (center.X - d.X);
            int y = (center.Y - d.Y);
            d2 += Mathf.Sqrt((x * x) + (y * y));
        }

        return d2 / _defects.Length;
    }

    private void findDefects()
    {
        if (_defects == null || _defects.Length < 1)
        {
            var hull = Cv2.ConvexHullIndices(poly);
            _defects = Cv2.ConvexityDefects(poly, hull);
        }
    }

    // public Point FindCenter()
    // {
    //     Moments M = Cv2.Moments(contourMat, true);
    //     int cX = (int)(M.M10 / M.M00);
    //     int cY = (int)(M.M01 / M.M00);
    // 
    //     return new Point(cX, cY);
    // }
}
