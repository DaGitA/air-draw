﻿using OpenCvSharp;
using UnityEngine;

namespace Assets.Scripts.Vision
{
    class Calibrator
    {
        private VideoFeedCapture feed;
        private Window calibration;
        private VisionController controller;
        private Mat lastFrame;
        private static Vector2 newSettingsPixel;
        private static bool shouldUpdateSettings;

        public Calibrator(VideoFeedCapture feed, VisionController controller)
        {
            this.controller = controller;
            this.feed = feed;
            calibration = new Window("calibration");
            calibration.OnMouseCallback += new CvMouseCallback(onClick);
        }

        public void Update()
        {
            lastFrame = feed.CaptureFrame();
            Cv2.ImShow("calibration", lastFrame);

            if (shouldUpdateSettings)
            {
                shouldUpdateSettings = false;
                controller.changeSettings(calibrationPhase((int)newSettingsPixel.x, (int)newSettingsPixel.y));
            }
        }

        private Settings calibrationPhase(int x, int y)
        {
            Mat median = lastFrame.MedianBlur(11);
            Mat imageToExtractColor = median.GaussianBlur(new Size(5, 5), 0);

            Mat HSVmat = new Mat(480, 640, MatType.CV_8UC3);
            Cv2.CvtColor(lastFrame, HSVmat, ColorConversionCodes.BGR2HSV);
            Vec3b pixel = HSVmat.At<Vec3b>(y, x);

            //Experimental values
            int hueLow = pixel[0] - 20;
            int satLow = pixel[1] - 50;
            int briLow = pixel[2] - 50;
            int hueHigh = pixel[0] + 20;
            int satHigh = pixel[1] + 50;
            int briHigh = pixel[2] + 50;

            showChoosenColor(x, y, imageToExtractColor);

            return new Settings(hueLow, hueHigh, satLow, satHigh, briLow, briHigh);
        }

        private static void showChoosenColor(int x, int y, Mat imageToExtractColor)
        {
            Vec3b pixelBGR = imageToExtractColor.Get<Vec3b>(y, x);
            Cv2.Circle(imageToExtractColor, 25, 25, 25, new Scalar(pixelBGR[0], pixelBGR[1], pixelBGR[2]), 50);
            Cv2.ImShow("ImageToExtractColor", imageToExtractColor);
        }

        private static void onClick(MouseEvent mouseEvent, int x, int y, MouseEvent me2)
        {
            if (mouseEvent == MouseEvent.LButtonDown)
            {
                newSettingsPixel = new Vector2(x, y);
                shouldUpdateSettings = true;
            }
        }

    }
}
